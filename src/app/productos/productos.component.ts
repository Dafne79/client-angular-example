import { ProductSummary } from './../product-detail/ProductDetail';
import { first } from 'rxjs/operators';
import { Component, OnInit } from '@angular/core';
import { ProductService } from 'src/app/services';
import { Router, NavigationEnd, ActivatedRoute } from '@angular/router'

@Component({
  selector: 'app-productos',
  templateUrl: './productos.component.html',
  styleUrls: ['./productos.component.css']
})
export class ProductosComponent implements OnInit {

  constructor(private productService: ProductService,
    private router : Router,
    private actRoute: ActivatedRoute) { 
    console.log("Iniciando peticiones desde OnInit");
  }

  ngOnInit(): void {
    
    console.log("Iniciando peticiones desde OnInit");
    //this.loadProducts();
  }

  function = this.actRoute.url.subscribe((data) => {
    console.log("Recargando tablas");
    this.loadProducts();  
  });

  ngOnActivate(){
    console.log("Iniciando peticiones desde OnActivate");
      
  }

  loadProducts(){
    this.productService.getAll()
    .pipe(first())
    .subscribe(data =>{
      console.log(data);
      this.products = data;
    },
    error => {
      console.log(error);
    }
    );
  }

  onDeleteProduct(id : number){
    this.productService.delete(id)
    .pipe(first())
    .subscribe(data => {
      console.log(data);
      this.loadProducts();
    },
    error => {
      console.log(error);
    });
  }

 
  products: ProductSummary[] = [];

  productsArrayIsEmpty(){
    return this.products.length == 0;
  }

}
