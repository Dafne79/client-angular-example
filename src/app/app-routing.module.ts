import { ProductosComponent } from './productos/productos.component';
import { NgModule, Component } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { UsuariosComponent } from './usuarios/usuarios.component';
import { PostsComponent } from './posts/posts.component';
import { ProductDetailComponent } from './product-detail/product-detail.component';

const routes: Routes = [
  {path: 'usuarios',component: UsuariosComponent},
  {path: 'posts',component: PostsComponent},
  {path: 'productos', component: ProductosComponent, children: [
    { path: 'ver/:id', component: ProductDetailComponent, data : { AddMode : false } },
    { path: 'agregar', component: ProductDetailComponent, data : { AddMode : true } },
  ] 
}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
