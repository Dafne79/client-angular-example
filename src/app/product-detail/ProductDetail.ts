export class ProductDetail {

    constructor(
      public id: number,
      public name: string,
      public status : string,
      public categoryId : number,
      public categoryName : string,
      public description: string,
      public stock: number
    ) {  }
  
  }

export class ProductSummary{

  constructor(
    public id: number,
    public name: string,
    public stock: number,
    public status: string
  ){}
}

export class ProductViewDetail{
  constructor(
    public id : number,
    public name : string,
    public categoryId : number,
    public categoryName : string,
    public description : string,
    public status : string,
    public stock : number
  ){}

}