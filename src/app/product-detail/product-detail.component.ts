import { ProductService } from 'src/app/services';
import { ProductDetail, ProductViewDetail } from './ProductDetail';
import { Component, OnInit, Renderer2, ViewChild, ElementRef, Input} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms'
import { Location } from '@angular/common';
import { first } from "rxjs/operators";


@Component({
  selector: 'app-product-detail',
  templateUrl: './product-detail.component.html',
  styleUrls: ['./product-detail.component.css']
})
export class ProductDetailComponent implements OnInit {
  productForm: any;
  productDetail!: ProductViewDetail;
  productId! : Number;
  AddMode : boolean = true;
  errorMessage! : {
    hasError : Boolean,
    message : String
  };

 

  constructor(private actRoute: ActivatedRoute,
    private location: Location,
    private renderer: Renderer2,
    private router: Router,
    private productService: ProductService
) { 
  
  }

  

  

  /** @description Ciclo de inicio de vida */
  ngOnInit() {
    this.errorMessage = { hasError : false, message : "" };
    // Verificamos en que modo se renderiza el componente
    this.AddMode = Boolean(this.actRoute.snapshot.data['AddMode'])
    console.warn(this.AddMode);
    // Si es modo agregar
    if(this.AddMode){
      // Inicializamos el formulario en vacio
      this.productForm = new FormGroup({
        id: new FormControl(''),
        name: new FormControl(''),
        category: new FormControl(''),
        description: new FormControl(''),
        status: new FormControl(''),
        stock: new FormControl('')
      });
      return;
    }
    // Obtenemos el id de la ruta
    this.productId = Number(this.actRoute.snapshot.paramMap.get('id'));
    console.log(this.productId);
    // Obtenemos la informacion del servicio
    this.productService.getById(this.productId)
    .pipe(first())
    .subscribe(data => {
      console.log(data);
      this.productDetail = data;
      
      this.productForm = new FormGroup({
        id: new FormControl(this.productDetail.id),
        name: new FormControl(this.productDetail.name),
        category: new FormControl(this.productDetail.categoryId),
        description: new FormControl(this.productDetail.description),
        status: new FormControl(this.productDetail.status),
        stock: new FormControl(this.productDetail.stock)
      });

    }, error => {
      console.log(error);
      this.router.navigate(['productos']);
    });
    //this.productDetail = new ProductDetail(1,"Nuevo","Disponible",5,"Farmacia","Nueva descripcion",20);
    console.log(this.productDetail);
    /*
    // Setteamos la informacion en el formulario
    this.productForm = new FormGroup({
      id: new FormControl(this.productDetail.id),
      name: new FormControl(this.productDetail.name),
      category: new FormControl(this.productDetail.categoryId),
      description: new FormControl(this.productDetail.description),
      status: new FormControl(this.productDetail.status),
      stock: new FormControl(this.productDetail.stock)
    });*/

    // Inicializamos el formulario en vacio
    this.productForm = new FormGroup({
      id: new FormControl(''),
      name: new FormControl(''),
      category: new FormControl(''),
      description: new FormControl(''),
      status: new FormControl(''),
      stock: new FormControl('')
    });
    
  }



  /**
   * @description Cierra el modal y retorna a la ruta anterior
   */
  onClose(){
    this.router.navigate(['productos']);
  }

  /**
   * @description Activa la edicion en el modal y habilita el guardado
   */
  onEdit(){
    if(this.productForm.invalid){
      return;
    }
    let newProduct = this.productForm.getRawValue();

    console.log(newProduct);
    this.productService.update(newProduct.id,newProduct.name,newProduct.category,newProduct.description,newProduct.stock)
    .subscribe(
      data => {
        console.log(data);
        this.router.navigate(['productos']);
      },
      error => {
        console.log(error.error.title);
        this.errorMessage = { hasError : true, message : error.error.title };
        console.log(this.errorMessage);
        setTimeout(() => { 
          this.errorMessage = {
          hasError : false,
          message : ""
        };},1000);
        console.log(this.errorMessage);
      }
    )

  }

  /**
   * @description Guarda la entidad agregada
   */
  onSave(){
    if(this.productForm.invalid){
      return;
    }

    let newProduct = this.productForm.getRawValue();

    console.log(newProduct);

    this.productService.createProduct(newProduct.name,newProduct.description,newProduct.category,newProduct.stock)
    //.pipe(first())
    .subscribe(
      data => {
        console.log(data);
        this.router.navigate(['productos']);
      },
      error => {
        console.log(error.error.title);
        this.errorMessage = { hasError : true, message : error.error.title };
        console.log(this.errorMessage);
        setTimeout(() => { 
          this.errorMessage = {
          hasError : false,
          message : ""
        };},1000);
        console.log(this.errorMessage);
      }
    );
  }

  ticketReceipt : any;


  categoryCollection = [
    {id:1, value:"Alimentos"},
    {id:2, value:"Deportes"},
    {id:3, value:"Casa"},
    {id:4, value:"Oficina"},
    {id:5, value:"Farmacia"},
];


}
