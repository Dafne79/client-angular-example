import { ProductDetail, ProductSummary, ProductViewDetail } from './../product-detail/ProductDetail';
import { environment } from './../../environments/environment.prod';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { MapOperator } from 'rxjs/internal/operators/map' 
import { map } from "rxjs/operators";

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  constructor(private http: HttpClient) { }

  private baseUrl = environment.baseUrl;

  /**
   * Crea un nuevo producto en la base de datos con los datos especificados
   * @param name 
   * @param description 
   * @param categoryId 
   * @param stock 
   * @returns 
   */
  createProduct(name:String, description:string,categoryId:number,stock:number){

    let newProduct = {
      name ,
      description,
      categoryId,
      stock
    }
    //console.info(newProduct);
    console.log(`${this.baseUrl}/Products`);
    
    //Realizamos la peticionnew
    return this.http.post<any>(`${this.baseUrl}Products`,newProduct, { observe: 'response' })
    .pipe(map(result => {
      console.info(result);
      return result;
    }));
  } 

  /**
   * 
   * @returns Obtiene todos los registros de productos en la base de datos
   */
  getAll(){
    return this.http.get<ProductSummary[]>(`${this.baseUrl}Products`)
    .pipe(
      map(result => {
        console.info(result);
        return result;
      }));
  }

  /**
   * Realiza la peticion de informacion para un id especifico
   * @param productId 
   * @returns 
   */
  getById(productId : Number){
    return this.http.get<ProductViewDetail>(`${this.baseUrl}Products/${productId}`)
    .pipe(map(result => {
      console.info(result);
      return result;
    }));
  }

  /**
   * Actualiza una entidad en la base de datos
   * @param id 
   * @param newName 
   * @param newCategory 
   * @param newDescription 
   * @param newStock 
   * @returns 
   */
  update(id : number, newName : string, newCategory : number, newDescription : string, newStock : number){
    let updtedProduct = {
      newName,
      newCategory,
      newDescription,
      newStock
    };
    return this.http.put<string>(`${this.baseUrl}Products/${id}`,updtedProduct)
    .pipe(map(result => {
      console.info(result);
      return result;
    }));
  }

  /**
   * Elimina una entidad por su Id
   * @param id 
   * @returns 
   */
  delete(id : number){
    return this.http.delete<string>(`${this.baseUrl}Products/${id}`)
    .pipe(map(result => {
      console.info(result);
      return result;
    }));
  }
}
